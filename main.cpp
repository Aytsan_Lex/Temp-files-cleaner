#include <iostream>
#include <exception>

#include "files_cleaner.hpp"



int main(int argc, char** argv)
{
  if (argc < 2)
  {
    std::cerr << "Usage: " << argv[0] << " <path to dir>\n";
    return 1;
  }

  const std::string_view target_directory_path{argv[1]};

  try
  {
    alex::utils::files_cleaner temp_files_cleaner{target_directory_path};

    const auto&& list_entities{temp_files_cleaner.list_of_entities_to_remove()};
    for (auto&& entity : list_entities)
    {
      std::cout << "Found: " << entity.path() << "\n";
    }

    const std::size_t removed_bytes{temp_files_cleaner.remove_all()};
    const double removed_KiB{static_cast<double>(removed_bytes / 1024)};
    const double removed_MiB{static_cast<double>(removed_KiB / 1024)};
    const double removed_GiB{static_cast<double>(removed_MiB / 1024)};

    std::cout << "Removed: " << removed_bytes << "b " << std::setprecision(5) << removed_KiB << "KiB " << removed_MiB << "MiB " << removed_GiB << "GiB\n";
  }
  catch (const std::exception& ex)
  {
    std::cerr << "Error: " << ex.what() << "\n";
    return 1;
  }

  return 0;
}

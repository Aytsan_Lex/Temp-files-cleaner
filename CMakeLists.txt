cmake_minimum_required(VERSION 3.5)

project(Temp-files-cleaner LANGUAGES CXX)

set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_FLAGS "-O2 -Wall -Wextra -Wpedantic")


set(__USE_STD_FILESYSTEM ON) # Use std::filesystem by default



file(GLOB_RECURSE SRC include/*.hpp src/*.cpp)
include_directories(${INCLUDE_DIRECTORIES} ./include/)

if(__USE_BOOST_FILESYSTEM)
    set(Boost_USE_STATIC_LIBS OFF)
    find_package(Boost REQUIRED COMPONENTS filesystem)

    if(Boost_FOUND)
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -D__USE_BOOST_FILESYSTEM")
        add_executable(${PROJECT_NAME} main.cpp ${SRC})
        target_link_libraries(${PROJECT_NAME} Boost::filesystem)
    endif()
elseif(__USE_STD_FILESYSTEM)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -D__USE_STD_FILESYSTEM")
    add_executable(${PROJECT_NAME} main.cpp ${SRC})
endif()

#pragma once

#include <string>
#include <list>

#ifdef __USE_BOOST_FILESYSTEM
# include <boost/filesystem.hpp>
namespace alex::utils { namespace fs = boost::filesystem; }
#elif defined (__USE_STD_FILESYSTEM)
# include <filesystem>
namespace alex::utils { namespace fs = std::filesystem; }
#endif // FILESYSTEM



namespace alex::utils::impl
{
  class files_cleaner_impl
  {
  public:
    files_cleaner_impl();
    files_cleaner_impl(std::string_view target_directory);
    ~files_cleaner_impl() = default;

    // No copy, no move
    files_cleaner_impl(files_cleaner_impl&&) = delete;
    files_cleaner_impl(const files_cleaner_impl&) = delete;
    files_cleaner_impl& operator=(const files_cleaner_impl&) = delete;
    files_cleaner_impl& operator=(const files_cleaner_impl&&) = delete;

    void set_target_directory(const fs::path& target_directory);
    std::string current_target_directory() const;
    std::size_t remove_all();

    std::list<fs::directory_entry> list_of_entities_to_remove() const;

  private:
    fs::path target_directory_;
    std::list<fs::directory_entry> entities_to_remove_;

    std::size_t scan_for_files_and_subdirs();
    void check_path(const fs::path& path);
  };
} // namespace alex::utils::impl
